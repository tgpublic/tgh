# A Higher-Order Temporal H-Index for Evolving Networks 

This repository contains the supplementary files for our KDD'23 paper
*A Higher-Order Temporal H-Index for Evolving Networks*.


## Data 

First, run `unzip data.zip` in the folder `data`.

The folder `data` then contains five of the data sets (subfolder `datasets`) 
and the data to compute the results for SIR simulation and the community detection.

All data sets are publicly available (see Table III for the sources and references). 
In order to use them with our tools, use the following data format.
The data set format is the following the number of nodes in the first line
followed by the chronologically ordered sequence of temporal edges 
`u v t tt` with u and v being nodes, t the timestamp of the edge, and tt the traversal time.

For example:
```
3
0 1 1 1
1 2 2 1
2 0 3 1
```
is a temporal graph with three nodes and three temporal edges.
The data sets included in the repository are already in this format.



## Compilation 
The algorithms are implemented in C++. 
To compile the executable, create a folder "release" in "source/tgh". 
Change into this folder and run

    cmake -DCMAKE_BUILD_TYPE=Release ..

Finally, run `make` to compile the code and get the executable file `tgh`.

## Usage

    ./tgh <dataset> -m=[0 for RECURS, 1 for STREAM] -n=[n-order] 


For example, `./tgh datasetname -m=1 -n=4` computes the 4-th order temporal H-index for the 
data set with name `datasetname` using the streaming algorithm.


## Make the SIR plots 

The folder `source/pyexp` contains the scripts for making the SIR plots. 
You can run it with `python3 make_sir_plots.py`.
