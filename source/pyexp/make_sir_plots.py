import pandas as pd
from matplotlib import pyplot as plt, rc
import seaborn as sns


def plot_results(dataset, betas, exts, names, outfile, legend=False):
    rc('font', **{'family': 'serif', 'serif': ['Helvetica'], 'size': 20})
    rc('text', usetex=True)

    dfs = []
    i = 0
    for ext in exts:
        d = dataset + "." + ext
        dfa = pd.read_csv(d, header=None)
        if len(dfa.columns) > 1:
            dfa.drop(0, axis=1, inplace=True)
            dfa.rename(columns={1: names[i]}, inplace=True)
        else:
            dfa.rename(columns={0: names[i]}, inplace=True)
        dfs.append(dfa)
        i += 1
    for beta in betas:
        d = dataset + ".sir" + str(beta)
        dfa = pd.read_csv(d, header=None)
        dfa.drop(0, axis=1, inplace=True)
        dfa.rename(columns={1: beta}, inplace=True)
        dfs.append(dfa)
        print(dfa)

    df = pd.concat(dfs, axis=1)
    df.reset_index()

    corr = df.corr(method='kendall')
    f, ax = plt.subplots(figsize=(8, 5))

    df = corr.drop(betas, axis=1)
    df = df.drop(names, axis=0)
    print(df)
    g = sns.lineplot(data=df, ax=ax, markers=True, linewidth=3)
    ax.grid()
    plt.setp(ax.get_legend().get_texts(), fontsize='12')  # for legend text

    if legend:
        plt.legend([], [], frameon=False)
    else:
        lgnd = plt.legend(prop={'size': 30}, loc='lower center', ncol=6, bbox_to_anchor=(1.5, -1.5))
        lgnd.legendHandles[0]._sizes = [400]
        lgnd.legendHandles[1]._sizes = [400]
        lgnd.legendHandles[2]._sizes = [400]
        lgnd.legendHandles[3]._sizes = [400]
        lgnd.legendHandles[3]._sizes = [400]
        for legobj in lgnd.legendHandles:
            legobj.set_linewidth(3.0)
        fig = lgnd.figure
        fig.canvas.draw()
        bbox = lgnd.get_window_extent()
        # bbox = bbox.from_extents(*(bbox.extents + np.array(expand)))
        bbox = bbox.transformed(fig.dpi_scale_trans.inverted())
        fig.savefig("legend_sir.pdf", dpi="figure", bbox_inches=bbox)

    plt.xlabel("$\\beta$")
    plt.ylabel("$\\tau$")
    plt.show()
    f.savefig(outfile, bbox_inches='tight')


def plot_only():
    names = ["college_und", "malawi", "infectious", "email-dnc_und"]

    for name in names:
        dataset = "../../data/sir/" + name + ".tg2.tgn"
        outfile = name + "_sir.pdf"

        plot_results(dataset, [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8],
                     ["hix32", "hix64", "deg", "sh",
                      "kc",
                      "tc", "twc", "gc", "gc2",
                      "ccp"],
                     ["\\textsc{Thi} (n=32)", "\\textsc{Thi} (n=64)",
                      "\\textsc{Degree}",
                      "\\textsc{Static H-index}",
                      "\\textsc{Static K-core}",
                      "\\textsc{Temp. Closeness}",
                      "\\textsc{Temp. Walk Centrality}",
                      "\\textsc{Gravity Centrality}",
                      "\\textsc{Local Gravity Centrality}",
                      "\\textsc{Coreness+}"
                      ],
                     outfile)


if __name__ == '__main__':
    plot_only()
